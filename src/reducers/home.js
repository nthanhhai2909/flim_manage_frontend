import { HOME } from "../contants/contants"
import { combineReducers } from "redux"

const home = (state = { manufacturers: [], flims: [] }, action) => {
  switch (action.type) {
    case HOME.SET_MANUFACTURER:
      return { ...state, manufacturers: action.manufacturers }
    case HOME.SET_FLIMS:
      return { ...state, flims: action.flims }
    default:
      return state
  }
}
export default combineReducers({
  home
})
