import React, { Component } from "react"
import Login from "../components/login"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { REQUEST_WITHOUT_AUTHENTICATION } from "../config/request"
import * as storage from "../config/storage"
import * as userActions from "../actions/user"

class LoginContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      res: ""
    }
  }

  submit = async form => {
    const { userActions, history } = this.props
    let res
    try {
      res = await REQUEST_WITHOUT_AUTHENTICATION.POST("/user/login", {
        email: form.email,
        password: form.password
      })
    } catch (err) {
      this.setState({ res: err.response.data.message })
      userActions.loginFail()
      return
    }
    storage.setToken(res.data.token)
    storage.setIdUser(res.data._id)
    userActions.loginSuceess()
    history.push("/")
  }

  render() {
    return <Login submit={form => this.submit(form)} res={this.state.res} />
  }
}
const mapDispatchToProps = dispatch => {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(
  null,
  mapDispatchToProps
)(LoginContainer)
