import React from "react"
import { Router, Route, Switch, Link } from "react-router-dom"
import history from "../history"
import HomeContainer from "./home"
import SignUp from "./signup"
import Login from "./login"
const App = () => (
  <Router history={history}>
    <Switch>
      <Route exact path="/" component={HomeContainer} />
      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/login" component={Login} />
    </Switch>
  </Router>
)
export default App
