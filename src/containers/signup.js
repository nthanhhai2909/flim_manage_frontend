import React, { Component } from "react"
import SignUp from "../components/signup"
import { REQUEST_WITHOUT_AUTHENTICATION } from "../config/request"

class SignUpContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      res: ""
    }
  }

  submit(form) {
    let res = REQUEST_WITHOUT_AUTHENTICATION.POST("/user/signup", {
      name: form.name,
      email: form.email,
      password: form.password,
      sex: form.sex
    })
      .then(res => this.setState({ res: res.data.message }))
      .catch(err => this.setState({ res: err.response.data.message }))
  }

  render() {
    return <SignUp submit={(form) => this.submit(form)} res={this.state.res} />
  }
}

export default SignUpContainer
