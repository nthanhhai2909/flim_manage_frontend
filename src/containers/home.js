import React from "react"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import Home from "../components/home"
import { REQUEST_WITHOUT_AUTHENTICATION, REQUEST_WITH_AUTHENTICATION } from "../config/request"
import { authentication } from "../config/auth"
import * as homeActions from "../actions/home"
class HomeContanier extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  async componentDidMount() {
    const { history, homeActions } = this.props
    if (!this.props.islogin) {
      if (!(await authentication())) {
        history.push("/login")
      }
    }

    // get manufacturers
    let res
    try {
      res = await REQUEST_WITHOUT_AUTHENTICATION.GET("/flim/manufacturers")
    } catch (err) {
      console.log(err)
    }
    homeActions.setManufacturer(res.data.manufacturers)
    // get all flims
    let resFlim
    try {
      resFlim = await REQUEST_WITHOUT_AUTHENTICATION.GET("/flim/flims")
    } catch (err) {
      console.log(err)
    }
    homeActions.setFlims(resFlim.data.flims)
  }

  async insert(form) {
    const { homeActions } = this.props
    let dataform = new FormData()
    dataform.set("name", form.name)
    dataform.append("img", form.img)
    dataform.append("director", form.director)
    dataform.append("manufacturer", form.manufacturer)
    if (!!form.date_start) {
      dataform.append("date_start", new Date(form.date_start))
    }
    if (!!form.date_release) {
      dataform.append("date_release", new Date(form.date_release))
    }
    REQUEST_WITH_AUTHENTICATION.POST("/flim/add", dataform)
      .catch(err => {
        this.setState({ res: "Insert error" })
        console.log(err)
      })
      .then( res => {
        this.setState({ res: "Insert success" })
        REQUEST_WITHOUT_AUTHENTICATION.GET("/flim/flims")
        .catch(err => console.log(err))
        .then(resFlim => homeActions.setFlims(resFlim.data.flims))
      })
  }

  delete(id) {
    const { homeActions } = this.props
    REQUEST_WITH_AUTHENTICATION.GET("/flim/delete", {id})
    .catch(err => console.log(err))
    .then(res => {
      REQUEST_WITHOUT_AUTHENTICATION.GET("/flim/flims")
      .catch(err => console.log(err))
      .then(resFlim => homeActions.setFlims(resFlim.data.flims))
    })
  }

  update(form) {
    const { homeActions } = this.props
    let dataform = new FormData()
    dataform.append("_id", form.id_selected)
    dataform.append("name", form.name)
    dataform.append("img", form.img)
    dataform.append("director", form.director)
    dataform.append("manufacturer", form.manufacturer)
    if (!!form.date_start) {
      dataform.append("date_start", new Date(form.date_start))
    }
    if (!!form.date_release) {
      dataform.append("date_release", new Date(form.date_release))
    }

    REQUEST_WITH_AUTHENTICATION.POST("/flim/update", dataform)
      .catch(err => {
        this.setState({ res: "Update error" })
        console.log(err)
      })
      .then( res => {
        this.setState({ res: "Update success" })
        REQUEST_WITHOUT_AUTHENTICATION.GET("/flim/flims")
        .catch(err => console.log(err))
        .then(resFlim => homeActions.setFlims(resFlim.data.flims))
      })
  }

  render() {
    return <Home
      insert={(form) => this.insert(form)}
      delete={(id) => this.delete(id)}
      update={(form => this.update(form))}
      res={this.state.res}
      {...this.props} />
  }
}

const mapStateToProps = state => ({
  islogin: state.user.login.login,
  manufacturers: state.home.home.manufacturers,
  flims: state.home.home.flims
})

const mapDispatchToProps = dispatch => {
  return {
    homeActions: bindActionCreators(homeActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContanier)
