export const TOKEN = "TOKEN"
export const ID_USER = "ID_USER"
export const USER = {
    LOGIN_SUCCESS: "LOGIN_SUCCESS",
    LOGIN_FAIL: "LOGIN_FAIL"
}

export const HOME = {
    SET_MANUFACTURER: "SET_MANUFACTURER",
    SET_FLIMS: "SET_FLIMS"
}