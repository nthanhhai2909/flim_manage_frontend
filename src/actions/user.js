import { USER } from "../contants/contants";

export const loginSuceess = () => ({
  type: USER.LOGIN_SUCCESS
});

export const loginFail = () => ({
  type: USER.LOGIN_FAIL
})