import {HOME} from "../contants/contants"

export const setManufacturer = (manufacturers) => ({
    type: HOME.SET_MANUFACTURER,
    manufacturers
})

export const setFlims = (flims) => ({
    type: HOME.SET_FLIMS,
    flims
})