import axios from "axios"
import { BASE_URL } from "./api"
import * as STORAGE from "../config/storage"
let authentication = axios.create({
  baseURL: BASE_URL,
})

let withoutauthentication = axios.create({
  baseURL: BASE_URL,
})

authentication.defaults.headers.common[
  "Authorization"
] = STORAGE.getToken()

export const REQUEST_WITH_AUTHENTICATION = {
  POST: async (url, body) => {
    return await authentication.post(url, body)
  },
  PUT: async (url, body) => {
    return await authentication.put(url, body)
  },
  DELETE: async (url, body) => {
    return await authentication.delete(url, body)
  },
  GET: async (url, params) => {
    return await authentication.get(url, { params: params})
  }
}


export const REQUEST_WITHOUT_AUTHENTICATION = {
  POST: async (url, body) => {
    return await withoutauthentication.post(url, body)
  },
  PUT: async (url, body) => {
    await withoutauthentication.put(url, body)
  },
  DELETE: async(url, body) => {
    return await withoutauthentication.delete(url, body)
  },
  GET: async (url, params) => {
    return await withoutauthentication.get(url, { params: params})
  }
}
