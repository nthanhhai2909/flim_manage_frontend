import React, { Component } from "react"
import _ from "lodash"
import {validateEmail} from "../utils/validation"
import {Link} from "react-router-dom"
class SignUp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      errorName: "",
      errorEmail: "",
      errorPassword: "",
      errorRepeatPassword: "",
      // for data form
      name: "",
      email: "",
      password: "",
      repeatPassword: "",
      sex: "male"
    }
  }

  submit() {
    // check name valid
    let valid = true
    if (!this.state.name) {
      this.setState({ errorName: "Name isn't empty" })
      valid = false
    } else {
      this.setState({ errorName: "" })
    }

    // check email invalid
    if (!validateEmail(this.state.email)) {
      this.setState({ errorEmail: "Email invalid" })
      valid = false
    } else {
      this.setState({ errorEmail: "" })
    }

    // check password and repeat password invalid
    if (this.state.password.length < 6) {
      this.setState({ errorPassword: "Password invalid" })
      valid = false
    } else {
      this.setState({ errorPassword: "" })
    }

    if (this.state.repeatPassword != this.state.password) {
      this.setState({ errorRepeatPassword: "Repeat password invalid" })
      valid = false
    } else {
      this.setState({ errorRepeatPassword: "" })
    }
    // submit to server
    if (valid) {
      this.props.submit(this.state)
      this.reset()
    }
  }

  reset = () => {
    this.setState({
      errorName: "",
      errorEmail: "",
      errorPassword: "",
      errorRepeatPassword: "",
      // for data form
      name: "",
      email: "",
      password: "",
      repeatPassword: "",
      sex: "male"
    })
  }

  render() {
    return (
      <div className="main">
        <section className="signup">
          <div className="container">
            <div className="signup-content">
              <div className="signup-form">
                <h2 className="form-title">Sign up</h2>
                <div className="register-form" id="register-form">
                  <div className="form-group">
                    <h6>{this.props.res}</h6>
                  </div>
                  <div className="form-group">
                    <center>{this.state.errorName}</center>
                    <input
                      type="text"
                      name="name"
                      id="name"
                      value={this.state.name}
                      placeholder="Your Name"
                      onChange={event =>
                        this.setState({ name: event.target.value })
                      }
                    />
                  </div>
                  <div className="form-group">
                    <center>{this.state.errorEmail}</center>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      value={this.state.email}
                      placeholder="Your Email"
                      onChange={event =>
                        this.setState({ email: event.target.value })
                      }
                    />
                  </div>
                  <div className="form-group">
                    <center>{this.state.errorPassword}</center>
                    <input
                      type="password"
                      name="pass"
                      id="pass"
                      value={this.state.password}
                      placeholder="Password"
                      onChange={event =>
                        this.setState({ password: event.target.value })
                      }
                    />
                  </div>
                  <div className="form-group">
                    <center>{this.state.errorRepeatPassword}</center>
                    <input
                      type="password"
                      name="re_pass"
                      id="re_pass"
                      value={this.state.repeatPassword}
                      placeholder="Repeat your password"
                      onChange={event =>
                        this.setState({ repeatPassword: event.target.value })
                      }
                    />
                  </div>
                  <div className="form-group form-button">
                    <input
                      type="submit"
                      name="signup"
                      className="form-submit"
                      value="Register"
                      onClick={() => this.submit()}
                    />
                  </div>
                </div>
              </div>
              <div className="signup-image">
                <figure>
                  <img
                    src={
                      process.env.PUBLIC_URL + "/assets/img/signup-image.jpg"
                    }
                    alt="sing up image"
                  />
                </figure>
                <Link className="signup-image-link" to={"/login"}>I am already member</Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default SignUp
