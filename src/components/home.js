import React, { Component } from "react"
export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      errorName: "",
      errorImgage: "",
      errorDateStart: "",
      errorDateRelease: "",
      errorManufacturer: "",
      name: "",
      img: null,
      director: "",
      manufacturer: "",
      date_start: null,
      date_release: null,
      url: "",
      id_selected: null,
    }
  }

  preCheck() {
    let valid = true
    if (!this.state.name) {
      this.setState({ errorName: "required" })
      valid = false
    } else {
      this.setState({ errorName: "" })
    }

    if (!this.state.img) {
      this.setState({ errorImgage: "required" })
      valid = false
    } else {
      this.setState({ errorImgage: "" })
    }

    if (!this.state.manufacturer) {
      valid = false
      this.setState({ errorManufacturer: "required" })
    } else {
      this.setState({ errorManufacturer: "" })
    }

    if (this.state.date_start) {
      if (new Date(this.state.date_start) > new Date()) {
        valid = false
        this.setState({
          errorDateStart: " must be smaller than the current date"
        })
      } else {
        this.setState({ errorDateStart: "" })
      }
    }

    if (this.state.date_release) {
      if (new Date(this.state.date_release) < new Date()) {
        valid = false
        this.setState({
          errorDateRelease: " must be more than the current date"
        })
      } else {
        this.setState({ errorDateRelease: "" })
      }
    }
    return valid
  }
  insert = () => {
    if (this.preCheck()) {
      this.props.insert(this.state)
      document.getElementById("image_post").value = ""
      this.reset()
    }
  }

  update(form) {
    let valid = true
    if (!this.state.name) {
      this.setState({ errorName: "required" })
      valid = false
    } else {
      this.setState({ errorName: "" })
    }

    if (!this.state.manufacturer) {
      valid = false
      this.setState({ errorManufacturer: "required" })
    } else {
      this.setState({ errorManufacturer: "" })
    }

    if (this.state.date_start) {
      if (new Date(this.state.date_start) > new Date()) {
        valid = false
        this.setState({
          errorDateStart: " must be smaller than the current date"
        })
      } else {
        this.setState({ errorDateStart: "" })
      }
    }

    if (this.state.date_release) {
      if (new Date(this.state.date_release) < new Date()) {
        valid = false
        this.setState({
          errorDateRelease: " must be more than the current date"
        })
      } else {
        this.setState({ errorDateRelease: "" })
      }
    }
    
    if (valid) {
      this.props.update(this.state)
      document.getElementById("image_post").value = ""
      this.reset()
    }
  }

  reset = () => {
    this.setState({
      errorName: "",
      errorImgage: "",
      errorDateStart: "",
      errorDateRelease: "",
      errorManufacturer: "",
      name: "",
      img: null,
      director: "",
      manufacturer: "",
      date_start: null,
      date_release: null,
      url: "",
      id_selected: null,
    })
  }

  handleChangeImg = imgfile => {
    if (imgfile === undefined)
      return
    let reader = new FileReader();
    reader.onloadend = () => {
      this.setState({
        img: imgfile,
        url: reader.result
      });
    };
    reader.readAsDataURL(imgfile);
  };

  select_update(element) {
    this.setState({
      name: element.name,
      director: element.director,
      manufacturer: element.manufacturer,
      date_start: !!element.date_start ? new Date(element.date_start).toISOString().slice(0,10) : null,
      date_release: !!element.date_release ? new Date(element.date_release).toISOString().slice(0,10) : null,
      url: element.img,
      id_selected: element._id,
    })
  }

  render() {
    return (
      <div >
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Admin</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <ul class="navbar-nav ml-auto">
            <button class="btn btn-outline-success my-2 my-sm-0 mr-auto" type="submit">Logout</button>
          </ul>

        </nav>
        <div className="container from-input-custom">
          <div className="row">
            <div className="col-md-8 offset-md-2">
              <div className="form-admin-input">
                <div className="item-form">
                  <p className="mess" style={{ textAlign: "center" }}>{this.props.res}</p>
                </div>
                <div className="item-form">
                  <span>Name</span>
                  <input type="text"
                    onChange={event => this.setState({ name: event.target.value })}
                    value={this.state.name}
                  />
                  <span className="mess">{this.state.errorName}</span>
                </div>
                <div className="item-form">
                  <span>Image</span>
                  <input type="file"
                    id="image_post"
                    onChange={event => this.handleChangeImg(event.target.files[0])}
                  />
                  <span className="mess">{this.state.errorImgage}</span>
                </div>
                <div className="item-form">
                  <span>Poster</span>
                  <img
                    src={this.state.url}
                    style={{ maxWidth: "300px" }}
                  />

                </div>
                <div className="item-form">
                  <span>Director</span>
                  <input type="text"
                    onChange={event => this.setState({ director: event.target.value })}
                    value={this.state.director}
                  />
                </div>
                <div className="item-form">
                  <span>Manufacturer</span>
                  <select
                    onChange={event => { this.setState({ manufacturer: event.target.value }) }}
                    value={this.state.manufacturer}>
                    <option selected="selected" value="">
                      Choose
                     </option>
                    {this.props.manufacturers.map((item, index) => {
                      return (
                        <option>{item}</option>
                      )
                    })}
                  </select>
                  <span className="mess">{this.state.errorManufacturer}</span>
                </div>
                <div className="item-form">
                  <span>Date Start</span>
                  <input type="date" style={{ width: "200px" }}
                    onChange={event => this.setState({ date_start: event.target.value })}
                    value={this.state.date_start}
                  />
                  <span className="mess">{this.state.errorDateStart}</span>
                </div>
                <div className="item-form">
                  <span>Date Release</span>
                  <input type="date" style={{ width: "200px" }}
                    onChange={event => this.setState({ date_release: event.target.value })}
                    value={this.state.date_release}
                  />
                  <span className="mess">{this.state.errorDateRelease}</span>
                </div>
                <button type="button" class="btn btn-primary" onClick={() => this.insert()}>Insert</button>
                <button type="button" class="btn btn-warning" onClick={() => this.update()}>Update</button>
              </div>
            </div>
          </div>
        </div>
        <div className="table-from">
          <table >
            <tr>
              <th>Name</th>
              <th>Director</th>
              <th>Manufacturer</th>
              <th>Date Start</th>
              <th>Date Release</th>
              <th>Actions</th>
            </tr>
            {this.props.flims.map((element, index) => {
              return (
                <tr>
                  <td>{element.name}</td>
                  <td>{element.director}</td>
                  <td>{element.manufacturer}</td>
                  <td>{element.date_start}</td>
                  <td>{element.date_release}</td>
                  <td>
                    <button type="button" style={{ margin: "5px" }} class="btn btn-danger"
                      onClick={() => this.props.delete(element._id)}
                    >Delete</button>
                    <button type="button" class="btn btn-success"
                      onClick={() => this.select_update(element)}
                    >Update</button>
                  </td>
                </tr>
              )
            })}
          </table>
        </div>
      </div>
    )
  }
}


